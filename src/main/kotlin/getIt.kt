package com.example.sandbox

/**
 * Подсчет номера витка
 * @param sum Номер элемента начала витка
 * @param i Номер витка
 * @param n Номер элемента
 * @param dim Размерность матрицы
 * @return Конечный номер витка
 */
fun getIt(sum: Int, i: Int, n: Int, dim: Int): Int {
    var res = 0
    if (sum >= n) {
        res = i - 1
    }
    if (sum <n) {
        res = getIt(sum + 4 * dim - 10 - 8 * i, i + 1, n, dim)
    }
    return res
}
