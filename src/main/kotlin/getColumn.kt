package com.example.sandbox

/**
 * Вычисление номера столбца по номеру элемента
 * @param p Номер элемента
 * @param dim Кол во столбцов
 * @return Номер столбца, начиная с 1
 */
fun getColumn(p: Int, dim: Int): Int {
    if ((p <= 0) or (dim <= 0) or (p > (dim * dim))) {
        throw IllegalArgumentException()
    }
    var n: Int = p
    var res: Int = -1
    if (n <= 3 * dim - 2) {
        val a: Int = dim
        val b: Int = 2 * dim - 1
        val c: Int = 3 * dim - 2
        if (n <a) res = 1
        if ((n >= a)and(n <= b)) res = n - a + 1
        if ((n> b)and(n <= c)) res = dim
    }
    if (n> 3 * dim - 2) {
        n -= (3 * dim - 2)
        val i: Int = getIt(0, 0, n, dim)
        val sum: Int = getSum(0, 0, n, dim)
        if (i % 2 == 0) {
            val a: Int = ((4 * dim - 8 * i - 6) / 4) + 1
            val b: Int = 2 * a - 2
            val c: Int = b + ((4 * dim - 8 * i - 6) / 4)
            val d: Int = c + ((4 * dim - 8 * i - 6) / 4) - 2
            n -= sum
            if (n <a) res = dim - i - 1
            if ((n >= a)and(n <= b)) res = dim - i - (n - a + 1)
            if ((n> b)and(n <c)) res = i + 2
            if ((n >= c)and(n <= d)) res = i + (n - c + 2)
        }
        if (i % 2 == 1) {
            val a: Int = ((4 * dim - 8 * i - 6) / 4)
            val b: Int = 2 * a
            val c: Int = b + a - 1
            val d: Int = c + a - 1
            n -= sum
            if (n <a) res = dim - i - n
            if ((n >= a)and(n <= b)) res = i + 2
            if ((n> b)and(n <c)) res = (n - b) + i + 2
            if ((n >= c)and(n <= d)) res = dim - i - 1
        }
    }
    return res
}
