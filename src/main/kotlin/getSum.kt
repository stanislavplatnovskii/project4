package com.example.sandbox

/**
 * Подсчет номера элемента где заканчивается i-1 виток
 * @param sum Номер элемента начала витка
 * @param i Номер витка
 * @param n Номер элемента
 * @param dim Размерность матрицы
 * @return Номер элемента: конец i-1 витка
 */
fun getSum(sum: Int, i: Int, n: Int, dim: Int): Int {
    var res = 0
    if (sum >= n) {
        res = sum - (4 * dim - 10 - 8 * (i - 1))
    }
    if (sum <n) {
        res = getSum(sum + 4 * dim - 10 - 8 * i, i + 1, n, dim)
    }
    return res
}
