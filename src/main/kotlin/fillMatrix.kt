package com.example.sandbox

interface Matrix {
    fun set(row: Int, column: Int, value: Int)
}

/**
 * Заполнение квадратной матрицы размерности n*n по правилу. Вариант 9 (1 неполный виток против часовой стрелки,
 * далее полные витки образующие кольца с чередованием (по часовой,и против часовой стрелки))
 * @param m Квадратная матрица
 * @param n Номер элемента
 */
fun fillMatrix(m: Matrix, n: Int) {
    if (n <= 0) {
        throw IllegalArgumentException("n<=0")
    }
    IntRange(1, n * n).forEach {
        m.set(getRow(it, n), getColumn(it, n), it)
    }
}
