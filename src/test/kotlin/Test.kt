package com.example.sandbox

import org.junit.jupiter.api.Test
import org.mockito.Mockito
import org.mockito.Mockito.mock

class Test {

    @Test
    fun testMatrixSize9() {
        val m = mock(Matrix::class.java)
        fillMatrix(m, 9)
        Mockito.verify(m).set(1, 1, 1)
        Mockito.verify(m).set(6, 1, 6)
        Mockito.verify(m).set(9, 6, 14)
        Mockito.verify(m).set(9, 9, 17)
        Mockito.verify(m).set(5, 9, 21)
        Mockito.verify(m).set(1, 9, 25)
        Mockito.verify(m).set(1, 4, 48)
        Mockito.verify(m).set(3, 7, 69)
        Mockito.verify(m).set(5, 6, 72)
        Mockito.verify(m).set(4, 5, 80)
        Mockito.verify(m).set(5, 5, 81)
    }
    @Test
    fun testMatrixSize5() {
        val m = mock(Matrix::class.java)
        fillMatrix(m, 5)
        Mockito.verify(m).set(1, 1, 1)
        Mockito.verify(m).set(2, 1, 2)
        Mockito.verify(m).set(3, 1, 3)
        Mockito.verify(m).set(4, 1, 4)
        Mockito.verify(m).set(5, 1, 5)
        Mockito.verify(m).set(5, 2, 6)
        Mockito.verify(m).set(5, 3, 7)
        Mockito.verify(m).set(5, 4, 8)
        Mockito.verify(m).set(5, 5, 9)
        Mockito.verify(m).set(4, 5, 10)
        Mockito.verify(m).set(3, 5, 11)
        Mockito.verify(m).set(2, 5, 12)
        Mockito.verify(m).set(1, 5, 13)
        Mockito.verify(m).set(1, 4, 14)
        Mockito.verify(m).set(2, 4, 15)
        Mockito.verify(m).set(3, 4, 16)
        Mockito.verify(m).set(4, 4, 17)
        Mockito.verify(m).set(4, 3, 18)
        Mockito.verify(m).set(4, 2, 19)
        Mockito.verify(m).set(3, 2, 20)
        Mockito.verify(m).set(2, 2, 21)
        Mockito.verify(m).set(1, 2, 22)
        Mockito.verify(m).set(1, 3, 23)
        Mockito.verify(m).set(2, 3, 24)
        Mockito.verify(m).set(3, 3, 25)
    }
    @Test
    fun testMatrixSize4() {
        val m = mock(Matrix::class.java)
        fillMatrix(m, 4)
        Mockito.verify(m).set(1, 1, 1)
        Mockito.verify(m).set(2, 1, 2)
        Mockito.verify(m).set(3, 1, 3)
        Mockito.verify(m).set(4, 1, 4)
        Mockito.verify(m).set(4, 2, 5)
        Mockito.verify(m).set(4, 3, 6)
        Mockito.verify(m).set(4, 4, 7)
        Mockito.verify(m).set(3, 4, 8)
        Mockito.verify(m).set(2, 4, 9)
        Mockito.verify(m).set(1, 4, 10)
        Mockito.verify(m).set(1, 3, 11)
        Mockito.verify(m).set(2, 3, 12)
        Mockito.verify(m).set(3, 3, 13)
        Mockito.verify(m).set(3, 2, 14)
        Mockito.verify(m).set(2, 2, 15)
        Mockito.verify(m).set(1, 2, 16)
    }
    @Test
    fun testMatrixSize3() {
        val m = mock(Matrix::class.java)
        fillMatrix(m, 3)
        Mockito.verify(m).set(1, 1, 1)
        Mockito.verify(m).set(2, 1, 2)
        Mockito.verify(m).set(3, 1, 3)
        Mockito.verify(m).set(3, 2, 4)
        Mockito.verify(m).set(3, 3, 5)
        Mockito.verify(m).set(2, 3, 6)
        Mockito.verify(m).set(1, 3, 7)
        Mockito.verify(m).set(1, 2, 8)
        Mockito.verify(m).set(2, 2, 9)
    }
    @Test
    fun testMatrixSize2() {
        val m = mock(Matrix::class.java)
        fillMatrix(m, 2)
        Mockito.verify(m).set(1, 1, 1)
        Mockito.verify(m).set(2, 1, 2)
        Mockito.verify(m).set(2, 2, 3)
        Mockito.verify(m).set(1, 2, 4)
    }
    @Test
    fun testMatrixSize1() {
        val m = mock(Matrix::class.java)
        fillMatrix(m, 1)
        Mockito.verify(m).set(1, 1, 1)
    }
    @Test
    fun testMatrixSize0() {
        val m = mock(Matrix::class.java)
        try {
            fillMatrix(m, 0)
        } catch (ex: IllegalArgumentException) {
            ex.message
        }
    }
}
